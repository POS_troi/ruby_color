# Ruby Color
Вывод цветного текста в консоль.

## GEM
[string_color](https://github.com/POStroi/ruby_string_color)

Использование:
```ruby
$LOAD_PATH << File.expand_path('../', __FILE__) #Add project folder to $LOAD_PATH, add subfolder ('../subfolder', __FILE__)
require 'color_string'
puts "This is color text".red
```
![ScreenShot](https://raw.githubusercontent.com/POStroi/Ruby_Color/master/console.png "ScreenShot")
